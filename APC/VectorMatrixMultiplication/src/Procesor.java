
public class Procesor {
	
	// Processor input and output variables
	public int mIn;
	public int vIn;
	public int mOut;
	public int vOut;
	public int result;
	
	// Procesor order number for matri to matrix multiplication
	public int x;
	public int y;
	
	public Procesor() {
		
	}
	
	public Procesor(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	// Step function
	public void step() {
		result += vIn * mIn;
		vOut = vIn;
		mOut = mIn;
	}
	
	// Print output for vector to matrix
	public String toString() {
		return "mIn=" + mIn + "|VECTOR_IN=" + vIn + "|mOut=" + mOut + "|vOut=" + vOut + "|RESULT=" + result; 
	}
	
	// Print output for matrix to matrix
	public String print() {
		return "m1In=" + mIn + "|m2In=" + vIn + "|m1Out=" + mOut + "|m2Out=" + vOut + "|RESULT=" + result;
	}
}
