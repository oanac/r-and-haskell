import java.util.Arrays;
import java.util.stream.Collectors;

public class MatrixMatrixMultiplication {
	
	public static int inputIndex = 0;
	
	public static void main(String[] args) {

		// Input 1
		int[][] matrixA = {{1,2,1,3},{1,-1,2,-2}};
		int[][] matrixB = {{1,2}, {3,4}, {2,4}, {1,0}};
		
		// Input 2
//		int[][] matrixA = {{1,2,1,3},{1,-1,2,-2},{4,3,2,1}};
//		int[][] matrixB = {{1, 1, 1},{2, 1, -1},{3, 2, -1}, {-2, 3, 5}};
		
		// Add delays for first matrix and transpose and add delays to the second matrix
		int[][] delayedMatrixA = constructMatrixWithDelays(matrixA, matrixA.length, matrixA.length-1);
		int[][] transposedB = transposed(matrixB);
		int[][] delayedMatrixB = transposed(constructMatrixWithDelays(transposedB, transposedB.length, transposedB.length-1));
		
		// Create processors
		Procesor[][] procs = createProcesors(matrixA, matrixB);
		
		// Execute algorithm
		while(MatrixMatrixMultiplication.inputIndex < procs.length + delayedMatrixB.length - 1) { // Stop condition
			
			System.out.println(System.lineSeparator() + "___=== Step number:" + (MatrixMatrixMultiplication.inputIndex + 1) + " ===___");
			
			// Propagate the input
			propagateInput(delayedMatrixA, delayedMatrixB, procs, MatrixMatrixMultiplication.inputIndex);

			// Step function
			Arrays.stream(procs).flatMap(Arrays::stream).collect(Collectors.toList()).forEach(Procesor::step);
			
			// Print out the results
			Arrays.stream(procs).flatMap(Arrays::stream).collect(Collectors.toList()).forEach(e -> System.out.print("P" + e.x + "|" + e.y + "->" + e.print() + System.lineSeparator()));
		}
		
	}
	
	// Get input elements from both matrixes and propagate the values from one processor to another
	private static void propagateInput(int[][] A, int[][] B, Procesor[][] procs, int inputIndex) {
		for (int i = 0; i < procs.length; i++) {
			for (int j = 0; j < procs[i].length; j++) {
				procs[i][j].mIn = j < procs[i].length -1 ? procs[i][j + 1].mOut : inputIndex < A[i].length ? A[i][inputIndex] : 0;
				procs[i][j].vIn = i < procs.length - 1 ? procs[i + 1][j].vOut : inputIndex < B.length ? B[inputIndex][j] : 0;
			}
		}
		
		MatrixMatrixMultiplication.inputIndex++;
	}

	// Create processors based on the size of the two matrixes
	private static Procesor[][] createProcesors(int[][] matrixA, int[][] matrixB) {
		Procesor[][] procs = new Procesor[matrixA.length][matrixB[0].length];
		for (int i = 0; i < procs.length; i++) {
			for (int j = 0; j < procs[i].length; j++) {
				procs[i][j] = new Procesor(i,j);
			}
		}
		return procs;
	}
	
	// Transpose the matrix and return the transposed matrix
	public static int[][] transposed(int[][] matrix) {
		int row = matrix.length;
		int column = matrix[0].length;
		int[][] transpose = new int[column][row];
        for(int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                transpose[j][i] = matrix[i][j];
            }
        }
		return transpose;
	}
	
	// Add the delays to the matrix (delays are represented by 0)
	private static int[][] constructMatrixWithDelays(int[][] matrix, int length, int delay) {
		int[][] delayedMatrix = new int[matrix.length][matrix[0].length + length];
		for (int i = 0; i < matrix.length; i++) {
			System.arraycopy(matrix[i], 0, delayedMatrix[i], delay, matrix[i].length);
			delay--;
		}
		return delayedMatrix;
	}

}
