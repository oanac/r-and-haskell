import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Processor {

	public int xin = 0;
	public int xout = 0;
	public int pin = 0;
	public int pout = 0;
	public int a = 0;

	public Processor(int a) {
		this.a = a;
	}

	public static void main(String[] args) {

		// Input coeficients and values
		Scanner tst = new Scanner(System.in);
		String coefsIn = inputCoefs(tst);
		String valuesIn = inputValues(tst);
		
		// Format the input
		List<Integer> poly = new ArrayList<>();
		List<Integer> x = new ArrayList<>();
		formatInput(coefsIn, poly);
		formatInput(valuesIn, x);
		

		// Setup initial values and result
		int nrProc = poly.size();
		int n = x.size();
		int inputIndex = 0;
		List<Integer> results = new ArrayList<>();

		// Construct number of needed processors, dependent on the number of coeficients
		List<Processor> processors = new ArrayList<>();
		for (int i = 0; i < nrProc; i++) {
			processors.add(new Processor(poly.get(i)));
		}

		// Polinomial evaluation
		while (true) {
			
			// Print output to user at each step
			if (inputIndex != 0) {
				processors.forEach(proc -> { printOutput(processors, proc);	});
				System.out.println();
			}
			
			// Initialize first processor with next value to process
			processors.get(0).pin = inputIndex < n ? 0 : 0;
			processors.get(0).xin = inputIndex < n ? x.get(inputIndex) : 0;
			inputIndex++;
			
			// Get the output from second processor and set it as input for previous processor
			for (int i = 1; i < processors.size(); i++) {
				processors.get(i).pin = processors.get(i - 1).pout;
				processors.get(i).xin = processors.get(i - 1).xout;
			}
			
			// Collect results if any
			if (processors.get(processors.size() - 1).pout != 0) {
				results.add(processors.get(processors.size() - 1).pout);
			}
			
			// Execute horner`s polinomial if we have input values
			step(processors);
			
			// Stop condition for polinomial evaluation
			if (processors.stream().allMatch(proc -> proc.pout == 0) && inputIndex > n) {
				break;
			}
		}
		results.forEach(rez -> System.out.println("Result -> " + rez));
	}

	private static void step(List<Processor> processors) {
		for (int i = 0; i < processors.size(); i++) {
			if (processors.get(i).xin != 0) {
				processors.get(i).pout = processors.get(i).pin * processors.get(i).xin + processors.get(i).a;
				processors.get(i).xout = processors.get(i).xin;
			} else {
				processors.get(i).pout = processors.get(i).xout = 0;
			}
		}
	}

	private static void formatInput(String input, List<Integer> result) {
		StringTokenizer tokens = new StringTokenizer(input, ",");
		while (tokens.hasMoreTokens()) {
			result.add(Integer.parseInt(tokens.nextToken().trim()));
		}
	}

	private static String inputValues(Scanner tst) {
		System.out.println("Input values for polinomial evaluation... (only numbers, comma separated followed by enter). No spaces!!!");
		System.out.println("Ex:3,2");
		String valuesIn = tst.nextLine();
		return valuesIn;
	}

	private static String inputCoefs(Scanner tst) {
		System.out.println("Input coeficients for polinomial evaluation... (only numbers, comma separated followed by enter). No spaces!!!");
		System.out.println("Ex:2,-6,2,-1");
		String coefsIn = tst.nextLine();
		return coefsIn;
	}

	private static void printOutput(List<Processor> processors, Processor proc) {
		System.out.print("P->" + processors.indexOf(proc) + "|");
		if (proc.pout != 0 && proc.xout != 0) {
			System.out.print("pIn : " + proc.pin + "|");
			System.out.print("xIn : " + proc.xin + "|");
			System.out.print("pOut : " + proc.pout + "|");
			System.out.print("xOut : " + proc.xout + "||");
		} else {
			System.out.print("pIn : " + 0 + "|");
			System.out.print("xIn : " + 0 + "|");
			System.out.print("pOut : " + 0 + "|");
			System.out.print("xOut : " + 0 + "||");
		}
	}
}