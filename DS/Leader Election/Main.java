package bullyAlgo;

import java.util.Scanner;

public class Main {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Number of processes:");
		int processes = in.nextInt();
		
		Algorithm[] t = new Algorithm[processes];

		for (int i = 0; i < processes; i++)
			t[i] = new Algorithm(new Process(i+1), processes);
		
		Election.initialElection(t);

		for (int i = 0; i < processes; i++)
			new Thread(t[i]).start();
	}
}
