Leader Election Algorithm (also called Bully Algorithm)

- you have to run the .jar file in one VM, then provide an input (number of total processes)
- then, the algorithm will run, selecting the leader (firstly, the process with the highest number is the leader); after a few steps, when this process stops responding, a new leader will be elected (a lower processor, with a higher value)
- when the highest processor "comes back", it will be elected as leader again, then it will lose its position when it will stop responding (and so on, the algorithm runs, until you stop it: CTRL+C)
