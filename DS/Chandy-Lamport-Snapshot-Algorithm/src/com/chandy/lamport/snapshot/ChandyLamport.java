package com.chandy.lamport.snapshot;

/**
 * Sorin Carcalicea
 * Chandy Lamport Snapshot Algorithm implementation
 */
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Random;
import java.util.Scanner;


public class ChandyLamport implements Runnable{

	static int port;
	static Process process;
	static ServerProperties serverProperties = null;
	Scanner scan = new Scanner(System.in);
	int maxAmount = 100;
	static int serverId;
	boolean snapshotStarted = false;
	static int marker = new Random().nextInt(1000-10) + 10;
	static String hostname;
	static ChandyLamport cl; 
	
	/**
	 * All initialization takes here.
	 * @throws UnknownHostException
	 */
	@SuppressWarnings("static-access")
	public ChandyLamport() throws UnknownHostException {
		port = 8400;
		hostname = InetAddress.getLocalHost().getHostAddress() + "";
		serverProperties = ServerProperties.getServerPropertiesObject();
		hostname += serverProperties.suffix;
		System.out.println("Starting process for: " + hostname);
		process = new Process(hostname);
		serverId = serverProperties.processId.get(hostname);

	}

	
	/**
	 * Defining rule for channels such that the same channel is used for communication between two processes every time.
	 * P0 -> P1 = 0
	 * P1 -> P2 = 1
	 * P2 -> P0 = 2
	 * P1 -> P0 = 3
	 * P0 -> P2 = 4
	 * P2 -> P1 = 5
	 * 
	 * @param sourceProcess - The process which is sending data.
	 * @param destProcess - The process which is going to receive the data.
	 * @return - The channel index that should be used.
	 */
	public int retrieveChannel(int sourceProcess, int destProcess){
		switch (sourceProcess) {
			case 0:
				return destProcess == 1 ? 0 : 4;
			case 1:
				return destProcess == 0 ? 3 : 1 ;
			case 2:
				return destProcess == 0 ? 2 : 5 ;
			default:
				return -1;
		}
	}

	/**
	 * Defining rule for channels such that the same channel is used for communication between two processes every time.
	 * P0 -> P1 = 0
	 * P1 -> P2 = 1
	 * P2 -> P0 = 2
	 * P1 -> P0 = 3
	 * P0 -> P2 = 4
	 * P2 -> P1 = 5
	 * 
	 * @param sourceProcess - The process which is sending data.
	 * @param destProcess - The process which is going to receive the data.
	 * @return - The channel index that should be used.
	 */
	public String retrieveHosts(int process){
		switch(process){
			case 0: 
				return "2,3";
			case 1: 
				return "0,5";
			case 2: 
				return "1,4";
			default:
				return "Invalid.";
		}
	}

	/**
	 * All the main execution happens here.
	 */
	@SuppressWarnings("static-access")
	public void run(){

		while(true){
		
			String threadName = Thread.currentThread().getName();
			if(!process.oneTime){
				sleepFor(3000);
				process.oneTime = true;
			}
			
			if(threadName.equals("local")) {
				
				//Pick a random index of the child.
				Random rand = new Random();
				int randomAmount = rand.nextInt(maxAmount + 1);
				int currentAmount = process.balance;
				int randomProcess = rand.nextInt(serverProperties.numberOfProcesses);
				sleepFor(3000);
				if(process.sendMarker){
					try{
						sendMarker(serverId, marker);
						process.sendMarker = false;
					} catch(Exception e) {
						break;
					}
				}

				while(randomProcess == serverId){
					randomProcess = rand.nextInt(serverProperties.numberOfProcesses);
				}

				String hostname = serverProperties.servers[randomProcess];
				if((currentAmount - randomAmount) > 0) {
					
					process.balance -= randomAmount;
					int channelIndex = retrieveChannel(serverId, randomProcess);
					try (Socket clientSocket = new Socket(hostname, port)) {
						DataOutputStream DO = new DataOutputStream(clientSocket.getOutputStream());
						DO.writeInt(channelIndex);
						DO.writeUTF("data");
						DO.writeInt(randomAmount);
					} catch (Exception e) {
						System.out.println("Client offline... " + hostname);
					} 
				}

				if(serverId == 1 && !process.sendMarker ){
					process.markerCompleted = false;
					sleepFor(10000);
					System.out.println("Taking Snapshot NOW!");
//					marker++;
					process.processState = process.balance;
					process.sendMarker = true;
					process.oneTimeMarker = true;
					process.stateRecorded = true;
				}
				
			} else if (threadName.equals("transferController")) {

				try (
						ServerSocket serverSocket = new ServerSocket(port);
						Socket clientSocket = serverSocket.accept();
					){
					
					String data = "";
					DataInputStream DI = new DataInputStream(clientSocket.getInputStream());
					int channelIndex = DI.readInt();
					data = DI.readUTF();

					if(data.equals("marker")) {
						
						marker = DI.readInt();
						if(!process.stateRecorded) {
							process.processState = process.balance;
							process.stateRecorded = true;
							process.sendingMarkers = true;
							process.channelReserved[channelIndex] = true;
							process.sendMarker = true;
						} else {
							process.channelReserved[channelIndex] = true;
							String[] sourceDest = retrieveHosts(serverId).split(",");
							int source = Integer.parseInt(sourceDest[0]);
							int dest = Integer.parseInt(sourceDest[1]);
							if(process.channelReserved[source] && process.channelReserved[dest]) {
								process.markerCompleted = true;
								process.balance += process.channels[source];
								process.balance += process.channels[dest];
								process.channels[source] = 0;
								process.channels[dest] = 0;
								process.channelReserved[source] = false;
								process.channelReserved[dest] = false;
								process.processState = 0; 
							}
						}
					} else if (data.equals("data")) {
						int randomAmount = DI.readInt();
						if(!process.stateRecorded) {
							System.out.println("Blue message on Channel" + channelIndex + " data: " + randomAmount);
							process.balance += randomAmount;
						} else {
							process.channels[channelIndex] += randomAmount;
							System.out.println("Red message on Channel" + channelIndex + " Amount: " + process.channels[channelIndex]);
						}
					} else {
						System.err.println("Invalid data.");
					}
				} catch(Exception e) {
					e.printStackTrace();
				} 
			} else {
				break;
			}
		}
	}


	private void sleepFor(int sleep) {
		try {
			Thread.sleep(sleep + 3000); 
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("static-access")
	private void sendMarker(int markerSource, int marker) throws IOException {
		int number = 1000;
		for(int server = 0; server < serverProperties.numberOfProcesses; server++) {
			if(server!= markerSource) {
					String hostname = serverProperties.servers[server];
					int channelIndex = retrieveChannel(markerSource, server);
				try (Socket clientSocket = new Socket(hostname, port)) {
					DataOutputStream DO = new DataOutputStream(clientSocket.getOutputStream());
					DO.writeInt(channelIndex);
					DO.writeUTF("marker");
					DO.writeInt(marker);
					sleepFor(number);
					number += 1000;
				} 
			}
		}
		process.sendingMarkers = false;
	}

	public static void main(String[] args) throws UnknownHostException {
		cl = new ChandyLamport();
		Thread t1 = new Thread(cl);
		t1.setName("local");
		Thread t2 = new Thread(cl);
		t2.setName("transferController");
		t1.start();
		t2.start();
	}

}
