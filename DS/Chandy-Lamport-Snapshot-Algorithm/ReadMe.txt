Deploy the application in cloud.

1. Run this command in order to copy the "application.jar" to all of your VMs:
  gcloud compute scp C:/application.jar vmdistributedsystems:/home/scarcalicea
			
	Change "vmdistributedsystems" with the name of your vm and the folder 
/home/scarcalicea to what you have in /home/ folder or copy it into a folder
where you have read/write rights. You need to run that command for VMs with
ip 10.128.0.2, 10.128.0.3 and 10.128.0.4. 

#############################################################################
######### The application is made to work only on this IPs. #################
#############################################################################

2. Start the application:
			java -jar application.jar
			
	You need to run this command on all 3 machines at the same time and from 
the folder where the jar is located. Then the machines will start to communicate 
and one of them, 10.128.0.3, will make a snapshot and send the token to all of 
the machines. Before the snapshot "blue messages" are sent and after the snapshot 
"red messages" are sent. When one of the machines is sending red messages then 
you can stop it and start it right back on. It will send blue messages because 
it does not have the tocken. 