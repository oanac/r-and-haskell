package com.ds.demo.controller;

/**
 * Implemented by Carcalicea Sorin
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.ds.demo.InternalState;
import com.ds.demo.model.HeartBeatData;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@CrossOrigin
public class Controller {
	
	public static boolean stop = false;
	
	/**
	 * URL - (GET) http://localhost:8080/stop<br>
	 * <br>
	 * Stop heartbeat for this machine<br>
	 */
	@RequestMapping(value = "/stop", method = RequestMethod.GET)
	public String stop() {
		Controller.stop = true;
		return "Stopped";
	}
	
	/**
	 * URL - (POST) http://localhost:8080/initialize/{flowControl} <br>
	 * <br>
	 * Initialize the initial state of all machines at the same time. <br>
	 * Construct the initial list for all machines. <br>
	 * <br>
	 * Values for flowControl: <br>
	 * 0 - gossip/all to all/ring heartbeat (secondary machines/secondary nodes(branches)) <br>
	 * 1 - gossip/all to all/ring heartbit (main machine/main node) <br>
	 * 2 - centralized <br>
	 * <br>
	 * @param initialState - list of ips<br>
	 */
	@RequestMapping(value = "/initialize/{flowControl}", method = RequestMethod.POST)
	public void init(@RequestBody InternalState initialState, @PathVariable String flowControl) {
		
		if(flowControl.equals("1")) {
			InternalState.initialize(initialState.getMachinesStates());
			for (String ip : initialState.getMachinesStates().keySet()) {
				try {
					initializeAll(ip, convertToJsonString(initialState));
				} catch (Exception e) {
					System.out.println("Ignore for now... Machine [" + ip + "] is down or does not exist!!!");
				}
			}
		} else if (flowControl.equals("2")) {
			InternalState.initialize(initialState.getMachinesStates());
		} else { // 0
			List<String> ips = new ArrayList<>();
			for (String ip : initialState.getMachinesStates().keySet()) {
				ips.add(ip);
			}
			
			new HeartBeatData().setIps(ips);
		}
	}

	/**
	 * URL - (GET) http://localhost:8080/isAlive<br>
	 * <br>
	 * Test if machine is up and running.<br>
	 * <br>
	 * @return - "Alive" if the machine is up and running<br>
	 */
	@RequestMapping(value = "/isAlive", method = RequestMethod.GET)
	public String isAlive() {
		return "Alive";
	}
	
	/**
	 * URL - (GET) http://localhost:8080/getState<br>
	 * <br>
	 * Query the machine state<br>
	 * <br>
	 * @return - internal machine state (list with all machine and number of heartbeat it receives)<br>
	 */
	@RequestMapping(value = "/getState", method = RequestMethod.GET)
	public @ResponseBody Map<String, AtomicInteger> getState() {
		return InternalState.getInstance().getMachinesStates();
	}
	
	/**
	 * URL - (GET) http://localhost:8080/getJSONState<br>
	 * <br>
	 * Query the machine state<br>
	 * <br>
	 * @return - internal machine state (list with all machine and number of heartbeat it receives)<br>
	 */
	@RequestMapping(value = "/getJSONState", method = RequestMethod.GET)
	public @ResponseBody InternalState getJSONState() {
		
		InternalState state = InternalState.getInstance();
		Map<String, AtomicInteger> stateCounter = new HashMap<>();
		stateCounter.put("192.168.0.1", new AtomicInteger(0));
		stateCounter.put("192.168.0.3", new AtomicInteger(0));
		stateCounter.put("192.168.0.5", new AtomicInteger(0));
		stateCounter.put("192.168.0.7", new AtomicInteger(0));
		stateCounter.put("192.168.0.9", new AtomicInteger(0));
		state.setMachinesStates(stateCounter);
		
		return state;
	}
	
	/**
	 * URL - (GET) http://localhost:8080/getJSONData<br>
	 * <br>
	 * Get sample JSON of HeartBeatData<br>
	 * <br>
	 * @return - heart beat data as JSON<br>
	 */
	@RequestMapping(value = "/getJSONData", method = RequestMethod.GET)
	public @ResponseBody HeartBeatData getHeartBeat() {
		
		HeartBeatData data = new HeartBeatData();
		List<String> ips = new ArrayList<>();
		ips.add("192.168.0.1");
		ips.add("192.168.0.3");
		ips.add("192.168.0.5");
		ips.add("192.168.0.7");
		ips.add("192.168.0.9");
		data.setIps(ips);
		data.setCurrentIndex(1);
		
		return data;
	}
	
	/**
	 * URL - (PUT) http://localhost:8080/put/heartbeat<br>
	 * <br>
	 * Send heartbeat to next machine (Ring/Gosip)<br> 
	 * <br>
	 * @param data - list with all machines and states<br>
	 */
	@RequestMapping(value = "/put/heartbeat", method = RequestMethod.PUT)
	public void putHeartBeat(@RequestBody HeartBeatData data) throws InterruptedException, JsonMappingException, JsonProcessingException {
		
		// Validate data
		List<String> ipList = new ArrayList<>();
		for (String ip : InternalState.machinesStates.keySet()) {
			ipList.add(ip);
		}

		if (ipList.size() < 1 || ipList.isEmpty()) {
			return;
		}
		
		// Update internal state
		InternalState.update(ipList.get(data.getCurrentIndex()));
		
		// Calculate the next index for next machine
		int index;
		if (data.getCurrentIndex() == ipList.size()-1) {
			data.setCurrentIndex(0); 
			index = 0;
		} else {
			index = data.getCurrentIndex() + 1;
		}
		
		// Send heart beat to next available machine
		int stopCounter = ipList.size(); // If all machines are not responding then there is no point in sending a heartbeat
		while (true) {
			
			if (stop) {
				Controller.stop = false;
				return;
			}
			
			try {
				String ip = ipList.get(index);
				if (isAlive(ip)) {
					data.setCurrentIndex(index);
					Thread.sleep(5000);
					sendState(ip, convertToJsonString(InternalState.getInstance()), "internalState");
					runAsync(() -> heartBeat(ip, convertToJsonString(data), "heartbeat"));
					break;
				}
				stopCounter--;
				index = index == ipList.size()-1 ? 0 : index+1;
				if (stopCounter == 0) {
					break;
				}
			} catch (Exception e) {
				System.out.println("Error in sending heartbeat or checking if system is online..");
				index = index == data.getIps().size()-1 ? 0 : index+1;
			}
		}
	}
	
	/**
	 * URL - (PUT) http://localhost:8080/put/allToAll<br>
	 * <br>
	 * Send heartbeat to next machine (All to All (send to all nodes))<br> 
	 * <br>
	 * @param data  - list of IPs<br>
	 */
	@RequestMapping(value = "/put/allToAll", method = RequestMethod.PUT)
	public void putAllToAll(@RequestBody HeartBeatData data) throws InterruptedException, JsonMappingException, JsonProcessingException {
		
		// Send heart beat to all members
		while (true) {

			if (stop) {
				Controller.stop = false;
				return;
			}
			
			try {
				for (int i = 0; i < data.getIps().size(); i++) {
					String ip = data.getIps().get(i);
					if (isAlive(ip)) {
						runAsync(() -> heartBeat(ip, convertToJsonString(data), "allToAll"));
					}
				}
				Thread.sleep(5000);
			} catch (Exception e) {
				System.out.println("Error in sending heartbeat or checking if system is online..");
			}
		}
	}
	
	/**
	 * URL - (PUT) http://localhost:8080/put/centralized<br>
	 * <br>
	 * Send heartbeat to central machine (Centralized)<br> 
	 * <br>
	 * @param data - list of IPs. First IP is the central machine IP and second IP is the IP from the machine which sends the heartbeat<br>
	 */
	@RequestMapping(value = "/put/centralized", method = RequestMethod.PUT)
	public void putCentralized(@RequestBody HeartBeatData data) throws InterruptedException, JsonMappingException, JsonProcessingException {
		
		// Send heartbeat to central machine
		while (true) {

			if (stop) {
				Controller.stop = false;
				return;
			}
			
			try {
				String ip = data.getIps().get(0);
				if (isAlive(ip)) {
					Thread.sleep(5000);
					runAsync(() -> heartBeat(ip, convertToJsonString(data), "centralNode"));
				}
			} catch (Exception e) {
				System.out.println("Error in sending heartbeat or checking if system is online..");
			}
		}
	}
	
	/**
	 * URL - (PUT) http://localhost:8080/put/centralNode<br>
	 * <br>
	 * Central node, collect data (Centralized)<br> 
	 * <br>
	 * @param data - list of IPs. First IP is the central machine IP and second IP is the IP from the machine which sends the heartbeat<br>
	 */
	@RequestMapping(value = "/put/centralNode", method = RequestMethod.PUT)
	public void putCentralNode(@RequestBody HeartBeatData data) throws InterruptedException, JsonMappingException, JsonProcessingException {
		
		// Update internal state
		String ip = data.getIps().get(1);
		InternalState.update(ip);
		System.out.println("Centralized -> Data received from: " + ip);
	}
	
	/**
	 * URL - (PUT) http://localhost:8080/put/internalState<br>
	 * <br>
	 * Central node, collect data (Centralized)<br> 
	 * <br>
	 * @param data - the list with machines and states which is received from other machine, processed and passed to the next machine<br>
	 */
	@RequestMapping(value = "/put/internalState", method = RequestMethod.PUT)
	public void putState(@RequestBody InternalState data) throws InterruptedException, JsonMappingException, JsonProcessingException {
		// Update internal state
		InternalState.machinesStates = data.getMachinesStates();
	}

	private boolean isAlive(String host) {
		
		System.out.println("START - Checking if host is alive: " + host);
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    
	    HttpEntity<String> result = null;
	    try {
	    	result = restTemplate.getForEntity("http://" + host + ":8080/isAlive", String.class);
	    } catch (Exception e) {
	    	System.out.println("Cannot communicate with client...");
	    }
    	
	    System.out.println("END - Checking if host is alive: " + host);
    	return result != null && result.getBody().equals("Alive");
	}
	
	private void heartBeat(String host, String JSONdata, String path) {
		
		System.out.println("START - Sending heartbeat: " + host);
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    
	    try {
	    	restTemplate.put("http://" + host + ":8080/put/" + path, new HttpEntity<String>(JSONdata, headers), String.class);
	    } catch (Exception e) {
	    	System.out.println("Cannot communicate with client...");
	    }
    	
	    System.out.println("END - Sending heartbeat: " + host);
	}
	
	private void sendState(String host, String JSONdata, String path) {
		
		System.out.println("START - Sending internal state: " + host);
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    
	    try {
	    	restTemplate.put("http://" + host + ":8080/put/" + path, new HttpEntity<String>(JSONdata, headers), String.class);
	    } catch (Exception e) {
	    	System.out.println("Cannot communicate with client...");
	    }
    	
	    System.out.println("END - Sending internal state: " + host);
	}
	
	private void initializeAll(String host, String JSONData) {
		
		System.out.println("START - InitializeAll: " + host);
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    
	    try {
    		restTemplate.postForEntity("http://" + host + ":8080/initialize/0", new HttpEntity<String>(JSONData, headers), String.class);
		} catch (Exception e) {
			System.out.println("Cannot communicate with client...");
		}
    	
	    System.out.println("END - InitializeAll: " + host);
	}
	
	private String convertToJsonString(Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			System.out.println("Error in converting to JSON");
		}
		return null;
	}
	
	private void runAsync(Runnable runAsynch) {
		new Thread(runAsynch).start();
	}

}
