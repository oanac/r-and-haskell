Prerequisites Java, Maven and Spring Tool Suite or Eclipse

In order to build the project you need to run the command:
   mvn clean install
The command must be executed from the demo folder, where the pom.xml file is located.

Sample usage:
- start the application like any other java application
      java -jar [nameOfTheJar].jar
      
- execute this steps in order to test the application
      just replace the ips with real ones

1. First initialize the data for all systems:

   Use link:   (POST) http://localhost:8080/initialize/1
   Sample data: 

  {
      "machinesStates": {
          "10.128.0.2": 0,
          "10.128.0.3": 0,
          "10.128.0.4": 0,
          "10.128.0.5": 0
      }
  }


2. Second step, start the heartbeat process

   Use link: (PUT) http://localhost:8080/put/heartbeat
   Sample data:

  {
      "ips": [
          "10.128.0.2",
          "10.128.0.3",
          "10.128.0.4",
          "10.128.0.5"
      ],
      "currentIndex": 0 -> Heartbeat will start from next index (1 in this case)
  }

Then the heartbeats are going to be sent from one machine to another and there will be an internal list which will keep count for all heartbeats.
If a machine is down, it is skipped and the heartbeat will be sent to the next available machine.

There are some utility links:
- (GET) http://localhost:8080/isAlive -> check if a machine is up and running before sending the heartbeat
- (GET) http://localhost:8080/getState -> get the current machine state
- (GET) http://localhost:8080/getJSONState -> get sample JSON state
- (GET) http://localhost:8080/getJSONData -> get sample JSON heartbeat data

3. Adding a new system:

   Use link:   (POST) http://localhost:8080/initialize/1
   Sample data: 

  {
      "machinesStates": {
          "192.168.0.1": 0,  -> If this line is missing then this system is removed
          "192.168.0.3": 0,
          "192.168.0.9": 0,
          "192.168.0.5": 0,
          "192.168.0.7": 0,
          "192.168.0.10": 0  -> This is the new system
      }
  }

Obs: you need to provide the existing members and new members at the same time.
Adding only new members will reset the internal list to the new list which was provided.

Copy the jar file from target folder to compute instance.
     gcloud compute scp C:/demo.jar vmdistributedsystems:/home/scarcalicea
     
Install java on compute instances(ssh into each instance):
     sudo apt-get install -yq openjdk-11-jdk git maven
     
Test instalation using this command: 
     java -version
     
If everything is fine, then you can run the application using this command:
     java -jar demo.jar
     
Something like this should appear in the command line:

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 
 and more lines
 2020-11-10 11:00:42.817  INFO 8444 --- [main] com.ds.demo.DemoApplication: Started DemoApplication in 4.639 seconds (JVM running for 5.469)
 
 Something like the last line, means a successful start-up
 
In order to test if the application is running correct you should take the instance ip and put this link in the browser:
    http://instance-ip:8080/isAlive
    
You also need to set-up the firewall in order to allow the communication on port 8080 with external network.

Find out how google cloud run

