package com.ds.demo.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.ds.demo.InternalState;
import com.ds.demo.model.HeartBeatData;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@CrossOrigin
public class Controller {
	
	/**
	 * Initialize the initial state of all machines at the same time.
	 * Construct the initial list for all machines. Initial index is 0.
	 * 
	 * @param initialState
	 */
	@RequestMapping(value = "/initialize/{mainNode}", method = RequestMethod.POST)
	public void init(@RequestBody InternalState initialState, @PathVariable String mainNode) {
		
		if(mainNode.equals("1")) {
			InternalState.initialize(initialState.getMachinesStates());
			for (String ip : initialState.getMachinesStates().keySet()) {
				try {
					initializeAll(ip, convertToJsonString(initialState));
				} catch (Exception e) {
					System.out.println("Ignore for now... Machine [" + ip + "] is down or does not exist!!!");
				}
			}
		} else {
			List<String> ips = new ArrayList<>();
			for (String ip : initialState.getMachinesStates().keySet()) {
				ips.add(ip);
			}
			
			new HeartBeatData().setIps(ips);
		}
	}

	/**
	 * Test if machine is up and running.
	 * 
	 * @return - "Alive" if the machine is up and running
	 */
	@RequestMapping(value = "/isAlive", method = RequestMethod.GET)
	public String isAlive() {
		return "Alive";
	}
	
	/**
	 * Query the machine state
	 * 
	 * @return - internal machine state (list with all machine and number of heartbeat it receives)
	 */
	@RequestMapping(value = "/getState", method = RequestMethod.GET)
	public @ResponseBody Map<String, Integer> getState() {
		return InternalState.machinesStates;
	}
	
	/**
	 * Get sample JSON of internal machine state
	 * 
	 * @return - sample JSON with internal machine state (list with all machine and number of heartbeat it receives)
	 */
	@RequestMapping(value = "/getJSONState", method = RequestMethod.GET)
	public @ResponseBody InternalState getJSONState() {
		
		InternalState state = new InternalState();
		Map<String, Integer> stateCounter = new HashMap<>();
		stateCounter.put("192.168.0.1", 0);
		stateCounter.put("192.168.0.3", 0);
		stateCounter.put("192.168.0.5", 0);
		stateCounter.put("192.168.0.7", 0);
		stateCounter.put("192.168.0.9", 0);
		state.setMachinesStates(stateCounter);
		
		return state;
	}
	
	/**
	 * Get sample JSON of HeartBeatData
	 * 
	 * @return - heart beat data as JSON
	 */
	@RequestMapping(value = "/getJSONData", method = RequestMethod.GET)
	public @ResponseBody HeartBeatData getHeartBeat() {
		
		HeartBeatData data = new HeartBeatData();
		List<String> ips = new ArrayList<>();
		ips.add("192.168.0.1");
		ips.add("192.168.0.3");
		ips.add("192.168.0.5");
		ips.add("192.168.0.7");
		ips.add("192.168.0.9");
		data.setIps(ips);
		data.setCurrentIndex(1);
		
		return data;
	}
	
	/**
	 * Send heartbeat to next machine
	 * 
	 * @param data
	 * @return
	 * @throws InterruptedException 
	 * @throws JsonProcessingException 
	 * @throws JsonMappingException 
	 */
	@RequestMapping(value = "/put", method = RequestMethod.PUT)
	public void put(@RequestBody HeartBeatData data) throws InterruptedException, JsonMappingException, JsonProcessingException {
		
		// Update internal state
		Integer currentIndex = data.getCurrentIndex();
		String prevIP;
		if (currentIndex == 0) {
			prevIP = data.getIps().get(data.getIps().size()-1);
		} else {
			prevIP = data.getIps().get(currentIndex - 1);
		}
		
		propagateState(prevIP);
		
		// Reset current state if we reach the end
		if (data.getCurrentIndex() == data.getIps().size()) data.setCurrentIndex(0); 
		
		// Send heart beat to next available machine
		for (int i = data.getCurrentIndex() + 1; i < data.getIps().size(); i++) {
			String ip = data.getIps().get(i);
			if (isAlive(ip)) {
				data.setCurrentIndex(i);
				Thread.sleep(5000);
				heartBeat(ip, convertToJsonString(data));
			}
		}
	}

	private boolean isAlive(String host) {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
    	HttpEntity<String> result = restTemplate.getForEntity("http://" + host + ":8080/isAlive", String.class);
    	return result.getBody().equals("Alive");
	}
	
	private void heartBeat(String host, String JSONdata) {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
    	restTemplate.put("http://" + host + ":8080/put", new HttpEntity<String>(JSONdata, headers), String.class);
	}
	
	private void initializeAll(String host, String JSONData) {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
    	restTemplate.postForEntity("http://" + host + ":8080/initialize/0", new HttpEntity<String>(JSONData, headers), String.class);
	}
	
	@SuppressWarnings("static-access")
	private void propagateState(String host) throws JsonMappingException, JsonProcessingException {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ResponseEntity<InternalState> prevState = restTemplate.getForEntity("http://" + host + ":8080/getState", InternalState.class);
		InternalState.machinesStates = prevState.getBody().machinesStates;
	}
	
	private String convertToJsonString(Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			System.out.println("Error in converting to JSON");
		}
		return null;
	}

}
