package com.ds.demo;

import java.util.HashMap;
import java.util.Map;

public class InternalState {
	
	public static Map<String, Integer> machinesStates = new HashMap<>();
	
	public static void initialize(Map<String, Integer> machines) {
		InternalState.machinesStates = machines;
	}
	
	public static void update(String ip) {
		Integer counter = InternalState.machinesStates.get(ip);
		if (counter != null) {
			counter = counter + 1;
			InternalState.machinesStates.put(ip, counter);
			System.out.println("Updated ip: " + ip + ", at counter value: " + counter);
		} else {
			machinesStates.put(ip, 1);
			System.out.println("Updated ip: " + ip + ", at counter value: " + 1);
		}
	}
	
	public static void remove(String ip) {
		if (InternalState.machinesStates.get(ip) != null) {
			InternalState.machinesStates.remove(ip);
			System.out.println("Removed ip: " + ip);
		}
	}

	public Map<String, Integer> getMachinesStates() {
		return InternalState.machinesStates;
	}

	public void setMachinesStates(Map<String, Integer> machinesStates) {
		InternalState.machinesStates = machinesStates;
	}

	
}
