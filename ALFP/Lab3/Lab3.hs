--- exercise 1 
type Picture =[[Char]]
-- examples of pictures with only characters in it type = Picture [[Char]]
horse :: Picture
horse = [".......##...",
         ".....##..#..",
         "...##.....#.",
         "..#.......#.",
         "..#...#...#.",
         "..#...###.#.",
         ".#....#..##.",
         "..#...#.....",
         "...#...#....",
         "....#..#....",
         ".....#.#....",
         "......##...."]
white :: Picture
white = ["...",
         "......",
         "......",
         "......",
         "......",
         "......"]
black :: Picture
black = ["######",
         "######",
         "######",
         "######",
         "######"
         ]
empty :: Picture
empty = []
notEmpty pict = pict /= []
-- checking if this picture is empty 
rectangular pict =
  notEmpty pict && 
  and [ length first == length l | l <-rest ]
  where
    (first:rest) = pict
    -- checking if the picture is symetric 

width, height :: Picture -> Int
height = length
width = length . head 
picSize :: Picture -> (Int,Int)
picSize pict
        | notEmpty pict == False = (0,0)
        | rectangular pict == True = (width pict, height pict) 
-- function which returns a tuple of rows and colomns of the picture which is rectangular 
-- for the white one will doesn't work 
-- picSize horse 
-- picSize black
-- picSize white
-- picSize empty
-- exercise 3 

scaleList :: [a] -> Int -> [a]
scaleList [] _ = [] 
-- the default 
scaleList (x:xs) n = [x | i <- [1..n]] ++ (scaleList xs n)
-- to generate each element on i times and concatenate with the rest of the list 
-- ['a' | i<-[1..10]] -> will produce "aaaaaaaaaa"
-- scaleList "abc" 3

-- Exercise 4

-- definition of scaleList lst n
-- scaleList is defined in Exercise 3

-- set scaleRow equivalent to scaleList
scaleRow = scaleList

-- scale function takes 2 arguments:
-- pic of type Picture and n of type Int
-- scale returns a picture 
scale :: Picture -> Int -> Picture

-- Lambda abstraction \row -> (scaleRow row n)
-- map((\row -> (scaleRow row n)) pic) applies the scaleRow function to the pic argument
-- scaleList is used to generate each specific row on n times and concatenate with the rest of the list
scale pic n = (scaleList (map ( \row -> (scaleRow row n)) pic) n)

-- Variable exPic of type Picture
exPic :: Picture
exPic = [ "#.#",
          "..#"]

-- Variable scaledPic of type Picture stores the scaled picture
scaledPic :: Picture
scaledPic = scale exPic 2

-- Exercise 6 (calculate triples based on constraints) From assignment
nextTriple :: (Int,Int,Int) -> (Int,Int,Int)
nextTriple (0,0,z) = (z+1,0,0)
nextTriple (0,x,y) = (x-1,0,y+1)
nextTriple (x,y,z) = (x-1,y+1,z)

triples :: [(Int,Int,Int)]
triples = (0,0,0):map nextTriple triples

-- a) If we compute the value of triples, then our program will run forever.
--    The list triples is an infinite list and if we want to display this value, then the program will run forever.
--    In this case we will need to force terminate it in order to finish the operation.


-- b) Return the n-th pitagorean tuple
pyth :: Int -> (Int, Int, Int)
pyth x = head (drop (x-1) (allPyth 1000))

allPyth :: Int -> [(Int, Int, Int)]
allPyth i = [(a, b, c) | a <- [0..i], b <- [0..a], c <- [0..i], (a^2) + (b^2) == (c^2)]

-- c) Indicate the first 6 Pythagorean tuples
firstSix :: [(Int,Int,Int)]
firstSix = take 6 (allPyth 5)

-- Exercise 7: Consider the Shape data type for geometric shapes which was defined in the Lecture Notes.
data Shape  = Circle Float | Rectangle Float Float | Triangle Float Float Float
-- Define the function
perimeter :: Shape -> Float
-- which takes as input a list of geometric shapes and returns its perimeter.
-- We know that in Math to calculate the Circle's perimeter is 2 * π * R
perimeter (Circle r) = 2 * pi * r
-- We know that in Math to calculate the Rectangle's perimeter is width + height + width + height, so we can say that it is 2 * height * width
perimeter (Rectangle h w) = 2 * h * w
-- We know that in Math to calculate the Triangle's perimeter is a + b +c
perimeter (Triangle a b c)= a + b + c
