%ex4 - write the function:
% f(X, Y, Z) = (X^100 − 3 ∗ Y ) + (5 ∗ Z + X ∗ Y )

function(X, Y, Z, Res):-
	Res is (X^100 - 3*Y) + (5*Z + X*Y).
