%ex 18

last([X], X):-!.
last([H|T], X):-
	last(T, X).


%whiteboard

%generate random list

generate_elem_list(Length, Interval, Result):-
	Length>0,
	genlist_acc(Length, Interval, Result, []).

genlist_acc(0, _, Acc, Acc):-!.
genlist_acc(Length, Interval, Result, Acc):-
	R is random(Interval),
	NewLength is Length-1,
	genlist_acc(NewLength, Interval, Result, [R|Acc]).

write_in_file(Path, L):-
	tell(Path),
	write_new_line(L),
	told.

%generate elements on newline
write_new_line([]):-!.
write_new_line([H|T]):-
	write(H), write('.'), nl,
	write_new_line(T).
	

%read from file
read_from_files(Path, L):-
	see(Path),
	read_file(X, [], L),
	seen.

read_file(X, L, R):-
	read(X),
	X \= end_of_file, !, 
	append(L, [X], R1),
	read_file(Y, R1, R).
read_file(end_of_file, R, R):-!.

%quicksort
quicksort([], []):-!.
quicksort([X], [X]):-!.
quicksort([H|T], Result):-
	split([H|T], L1, L2), !,
	quicksort(L1, Result1), !,
	quicksort(L2, Result2), !,
	append(Result1, Result2, Result).

split([H], [], [H]):-!.
split([H1, H2|T], [H2|L1], L2):-
	H1>=H2, !,
	split([H1|T], L1, L2), !.
split([H1, H2|T], L1, [H2|L2]):-
	split([H1|T], L1, L2), !.
	