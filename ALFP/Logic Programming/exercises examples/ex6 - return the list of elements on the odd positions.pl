%ex6 - return the list of elements on the odd
%positions from a list of integers

%place the cut predicate so it stops at one element
%and dont get the false
odd_pos([], []).
odd_pos([X], [X]):-!.

odd_pos([H1,H2|T1], [H1|T2]):-
	odd_pos(T1, T2).