%ex7 - return the list of elements on the even
%positions from a list of integers

even_pos([], []).
even_pos([X], []):-!.

even_pos([H1,H2|T1], [H2|T2]):-
	even_pos(T1, T2).