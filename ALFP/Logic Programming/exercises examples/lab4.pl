%reading elements and doing the sum
example:-see('C:\\Users\\Alex\\Documents\\Prolog\\pb.txt'),
    read(X),read(Y),read(Z),read(V), sum([X,Y,Z,V],W),
    write('the sum of the elements from the file pb.txt is: '),write(W), seen.
sum([],0).
sum([X|T],W):-sum(T,S),W is X+S.

%merge sort again
% split list
split([],[],[]):-!.
split([X],[],[X]):-!.
split([H1,H2|T],[H1|T2],[H2|T3]):-split(T,T2,T3).



%  merging
merge([],L2,L2):-!.
merge(L1,[],L1):-!.
merge([H1|T1],[H2|T2],[H1|T3]):-H1=<H2,!,merge(T1,[H2|T2],T3).
merge([H1|T1],[H2|T2],[H2|T3]):-H2<H1,merge([H1|T1],T2,T3).

%merge sort
mergesort([],[]):-!.
mergesort([X],[X]):-!.
mergesort(List,Res):-
    split(List,L1,L2),!,
    mergesort(L1,S1),
    mergesort(L2,S2),
    merge(S1,S2,Res).

%reading and merging numbers from file
merge_file1:-see('C:\\Users\\Alex\\Documents\\Prolog\\mergesort.txt'),
    read(X),read(Y),read(Z),read(A),read(B),read(C), mergesort([X,Y,Z,A,B,C],R),
    seen,
    tell('C:\\Users\\Alex\\Documents\\Prolog\\mergesort_write.txt'),write('the sorted list from the file mergesort.txt is: '),write(R),told.

%generate a list, sort it, display it
merge_file2:-generate_elem_list1(10,5,L),
    mergesort(L,R),
    tell('C:\\Users\\Alex\\Documents\\Prolog\\mergesort_generate_write.txt'),write('the sorted list from the file mergesort.txt is: '),write(R),told.

%generate random list
genlist_acc(0, _, Acc, Acc):-!.
genlist_acc(Length, Interval, Result, Acc):-
    R is random(Interval),
    NewLength is Length-1,
    genlist_acc(NewLength, Interval, Result, [R | Acc]).

generate_elem_list1(0, _, []).
generate_elem_list1(Length, Interval, Result):-
    Length>0,
    genlist_acc(Length, Interval, Result, []).
	
%left shift for 4 numbers
left_shift:-see('C:\\Users\\Alex\\Documents\\Prolog\\left_shift.txt'),read(X),read(Y),read(Z),read(V),
seen, tell('C:\\Users\\Alex\\Documents\\Prolog\\left_shift_write.txt'),write(Y),write(Z),write(V),write(X),told.

%even and odd problem	
get_list(W.L):-read(X),append([W],[Ws],Res),!,get_list(Res,L).
get_list1(Ws,Ws).

	
even_odd:-see('C:\\Users\\Alex\\Documents\\Prolog\\even_odd.txt'),get_list(L),write(L).



