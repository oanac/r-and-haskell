/**
4. Deletes the duplications and prints the reverse list (uses accumulator and the cut predicate).

deldup(L, X):− dupacc(L, [], X).
dupacc([ ], A, A).
dupacc([H|T], A, L):− member(H,A), !, dupacc(T, A ,L).
dupacc([H|T], A, L):− dupacc(T, [H|A], L).

Modify the predicate such that it will delete the duplications from a list and returns the list (use accumulator and the cut ! ):
?− deldup2([1,1,1,2,2,3,4,5,4],L).
L = [1,2,3,4,5].
*/

% we just append
deldup2(L,X):- dupacc2(L,[],X ).
dupacc2([], A,A).
dupacc2([H|T],A,L):- member(H,A), !, dupacc2(T,A,L).
dupacc2([H|T],A,L):- append(A, [H], Y), dupacc2(T,Y,L).