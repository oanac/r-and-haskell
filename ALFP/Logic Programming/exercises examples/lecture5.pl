% operators in prolog (from lecture 5)
% test the operators on true and false, like true\/false/\true
% lower precedence binds the strongest

% not 
:-op(100, fx, ~).
~X:- X, !, fail.
~X.


% conjunction
:-op(150, xfy, /\).
X/\Y:- X, Y.


% disjunction
% if X is true, stop and return true, else go to Y and return it
:-op(150, xfy, \/).
X\/Y:- X, !; Y.

% implication  (false only if true=>false)
% we use the known rule from logic
:-op(50, yfx, =>).
X=>Y:- ~X\/Y.

% equivalence
:-op(50, xfy, <=>).
X<=>Y:- (X=>Y) /\ (Y=>X).
