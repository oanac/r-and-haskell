%1
animal(duck).
animal(cat).
animal(dog).
animal(horse).

swim(duck).
swim(dog).

r(X):-animal(X),swim(X),!,fail.
r(X):-animal(X).


%2
no(P):- P, !, fail.
no(P). % or no(P):-!.

%3
friend(radu, elena).
friend(elena, mihai).
friend(zoe, viorel).

unfriend(X, Y):-
	friend(X, Y), !, fail.
unfriend(X, Y).

%4
% when we have = with a variable and a constant, the unification takes place, and we also have a substitution
%matching: _ = green(Y).
%_ -> variables, rules, constants, predicates, structures (lists)...
%  -> unifies with everything
%[_] -> a list containing one single element, list[H|T], list(green(Y))
%[X, X|_] -> the first 2 elements must be the same


%5
days([monday, tuesday, wednesday, thursday]).
next(X, Y):-
	List = [H1, H2|_],
	X = H1,
	Y = H2.