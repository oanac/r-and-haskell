%ex2 - reverse of a list

% a) without accumulators
rev1([], []).
rev1([H|T], Res):-
	rev1(T, T1),
	append(T1, [H], Res).


% b) with accumulators
rev2(List, Res):-
	revAcc(List, [], Res).

revAcc([], Acc, Acc).
revAcc([H|T], Acc, Res):-
	revAcc(T, [H|Acc], Res).
