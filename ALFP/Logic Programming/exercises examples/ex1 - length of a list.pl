%ex1 - write a predicate which returns the length of a list

% a) without accumulator
lengthy([], 0).
lengthy([H|T], X):-
	lengthy(T, X1),
	X is X1+1.


% b) with accumulator
len2(List, Result):-
	lenAcc(List, 0, Result).

lenAcc([], Acc, Acc).
lenAcc([H|T], Acc, Res):-
	NewAcc is Acc+1,
	lenAcc(T, NewAcc, Res).


% we can write [_|T], because we do not use H, and we no longer
% get the warning with singleton variable (unused)