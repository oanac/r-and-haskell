%sum of odd elements

%without accumulators
sumOddEl([], 0):-!.
sumOddEl([H|T], S):- 1 is H mod 2, !, sumOddEl(T, S1), S is S1+H.
sumOddEl([H|T], S):- sumOddEl(T, S).

%with accumulators
sumOddEl2(L, S):- sumAcc(L, 0, S).
sumAcc([], Acc, Acc):-!.
sumAcc([H|T], Acc, S):- 1 is H mod 2, !, NewAcc is Acc+H, sumAcc(T, NewAcc, S).
sumAcc([H|T], Acc, S):- sumAcc(T, Acc, S).



%list of odd elements - NOT WORKING
oddEl([], []).
oddEl([H1|T1], L2):- 1 is H1 mod 2, !, oddEl(T1, L2p), L2 is append(L2p, H1).
oddEl([H1|T1], L2):- oddEl(T1, L2).

%odd([], []).
%odd(List, Odd):- odd_acc(List, [], Odd).
%odd_acc([], Acc, Acc).
%odd_acc([H|T], Acc, Odd):- 1 is H mod 2, !, append(H, Acc),
%							odd_acc(T, NewAcc, Odd).
%odd_acc([H|T], Acc, Odd):- odd_acc(T, Acc, Odd).

