/**
? − member(1, [2, 1, 1, 3, 1, 4, 1]), will return one single solution,
once true or false -> use the cut predicate
*/

member(X, [X|_] ):-!.
member(X, [_|B] ):- member(X,B).
