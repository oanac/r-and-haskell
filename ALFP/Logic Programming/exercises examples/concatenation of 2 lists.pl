% concatenation of 2 lists

concat([], L, L).
concat([H1|T1], L2, [H1|R]):-
	concat(T1, L2, R).
