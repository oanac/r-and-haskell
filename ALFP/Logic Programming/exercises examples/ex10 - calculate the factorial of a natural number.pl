%10 - calculate the factorial of a natural number

factorial(0, 1):-!.
factorial(X, Res):-
    X1 is X-1,
    factorial(X1, ResNew),
    Res is ResNew*X.