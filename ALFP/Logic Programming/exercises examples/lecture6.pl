% exercises from lecture 6 (the solution are also found in lecture 7N)

/**
 1. Write a predicate to swap the 1st and 3rd element in a list.
Shorter lists are untouched.
?- swap_1_3([1,2,a,3], X).
*/
swap_1_3([], []).
swap_1_3([A], [A]):-!.
swap_1_3([A, B], [A, B]):-!.
swap_1_3([H1, H2, H3 | T], [H3, H2, H1 | T]).


% 4. Determine the minimum element in a list.
min_elem([], "Empty list").
min_elem([X], X):-!.

min_elem([H1,H2|T], R):-
	H1 < H2, 
	!,
	min_elem([H1|T], R).

min_elem([H1,H2|T], R):-
	H2 =< H1,
	!,
	min_elem([H2|T], R).

% 34. Partition of a list in 3 lists, of negative, odds, evens numbers.
split_list([],[],[],[]).
split_list([H|T],[H|R],Odd,Even):-H<0, !, split_list(T,R,Odd,Even).
split_list([H|T],R,[H|Odd],Even):- X is H mod 2, X=1, !, split_list(T,R,Odd,Even).
split_list([H|T],R,Odd,[H|Even]):- split_list(T,R,Odd,Even).