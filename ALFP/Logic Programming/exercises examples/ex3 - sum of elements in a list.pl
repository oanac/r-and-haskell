%ex3 - sum of elements in a list

sum([], 0).
sum([H|T], Sum):-
	sum(T, X),
	Sum is X+H.