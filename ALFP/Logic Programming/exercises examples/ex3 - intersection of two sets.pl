/**
3. The predicate which returns the intersection of two sets

inters([], X, []).
inters([X|T], Y, [X|Z]):- 
	member(X, Y),  inters(T,Y,Z),  !.
inters([X|T], Y, Z):- 
	inters(T,Y,Z).

   What happens if we do not use the cut predicate?
   What happens if we use the predicate cut in other place? Like in the
boundary condition?
*/

inters([], X, []):-!.
inters([X|T], Y, [X|Z]):- 
	member(X, Y),  inters(T,Y,Z).
inters([X|T], Y, Z):- 
	inters(T,Y,Z).
/**
- if we do not use cut, we get intermediate results 
(not all at the same time)
*/