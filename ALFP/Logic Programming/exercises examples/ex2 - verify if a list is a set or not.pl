/**
2. Verify if a list is a set or not (the predicate not)
 -> place the paranthesis for not(...)
*/

member(X, [X|_] ):-!.
member(X, [_|B] ):- member(X,B).

is_set([]).
is_set([X|T]):- 
	not(member(X, T)),
	is_set(T).