% Lab4
%ex 11

el_on_pos([H|T], 1, H):-!.
el_on_pos([H|T], N, X):-
	N1 is N-1,
	el_on_pos(T, N1, X).

remove_one(1, [H|T], T):-!.
remove_one(N, [H|T], [H|R]):-
	N1 is N-1,
	remove_one(N1, T, R).

remove_at(X, [H|T], N, R):-
	el_on_pos([H|T], N, X),
	remove_one(N, [H|T], R).

add_to_end(El, [], [El]):-!.
add_to_end(El, [H|T], [H|T1]):-
	add_to_end(El, T, T1).

remove_one_acc(N, [H|T], Acc, R):-
	N1 is N-1,
	add_to_end(H, Acc, NewAcc),
	remove_one_acc(N1, T, NewAcc, R).

remove_nth([H|T], I, N, R):-
	I mod N == 0,
	I1 is I+1,
	remove_nth(T, I1, N, R).
remove_nth([H|T], I, N, [H|T]):-
	I1 is I+1,
	remove_nth(T, I1, N, R).