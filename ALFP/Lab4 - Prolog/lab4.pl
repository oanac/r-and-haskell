% lab4
% many times "_" is used, instead of "H" or "T", to avoid the warning "Singleton variables"

% EXERCISE 1
% compareLists(L1, L2, R)
compareLists([], [], 0).
compareLists([], [_|_], -1).
compareLists([_|_], [], 1).
compareLists([_|T1], [_|T2], R):-
	compareLists(T1, T2, R).



% EXERCISE 2 
% separate(L,R1,R2)
separate([], [], []).
separate([H|T], [H|R1], R2):- separate(T, R1, R2), H < 10.
separate([H|T], R1, [H|R2]):- separate(T, R1, R2), H >= 10.
	


% EXERCISE 3
% avg_mean(List, M)
% sum of elements, which is divided by the length of the list

% sum
sum([], 0).
sum([H|T], Total):-
	sum(T,R1), 
	Total is H+R1.

% length
lengthList([], 0).
lengthList([_|T], Len):-
	lengthList(T, N1), 
	Len is N1+1.

% average mean
avg_mean(L, R):-
	lengthList(L, Len), 
	sum(L, Sum), 
	R is Sum / Len.

	

% EXERCISE 4
% minim(L,M)
minim([Min], Min).
minim([H|T], Min):- minim(T, Min), Min =< H.
minim([H|T], H):- minim(T, Min), H < Min.



% EXERCISE 5
% maxim(L,M)
maxim([Max], Max).
maxim([H|T], Max):- maxim(T, Max), Max >= H.
maxim([H|T], H):- maxim(T, Max), H > Max.



% EXERCISE 6
% swap_every_2(L,M)
swap_every_2([X], [X]).
swap_every_2([X,Y], [Y,X]).
swap_every_2([H1,H2|T1], [H2,H1|T2]):-
	swap_every_2(T1, T2).



% EXERCISE 7
% the cut predicator (!) is used, such that when those cases happen, the program stops (at false)
% flat_list(L)
flat_list([]).    
flat_list([[]|_]):- !, false.
flat_list([[_|_]|_] ):- !, false.
flat_list([_|T] ):- flat_list(T).



% EXERCISE 8
% rename it to avoid conflict of predefined function
% prefix2(P,L)
prefix2([], _).
prefix2([X|T], [X|T1]):-
	prefix2(T, T1).


% EXERCISE 9
sufix(Xs,Xs).
sufix([_|Ys],Xs) :- sufix(Ys,Xs).
% sufix([a,b,c],[b,c]). --> true
% sufix([a,b,c],[b,a]). --> false


% EXERCISE 10
sublist( S, L ) :-
    append( [_, S, _], L ).
% sublist([c,d,e],[a,b,c,d,e,f]).



% EXERCISE 11
% combineS(List1,List2,List) 
combineS([], [], []).
combineS([X], [], [X]).
combineS([], [X], [X]).
combineS([H1|T1], [H2|T2], [H1|X]):-
	combineS(T1, [H2|T2], X), H1 =< H2.
combineS([H1|T1], [H2|T2], [H2|X]):-
	combineS([H1|T1], T2, X), H2 < H1.



% EXERCISE 12
% delete_at(X, L, N, R)
delete_at(H, [H|T], 1, T).
delete_at(X, [H|T1], N, [H|T2]):-
	delete_at(X, T1, N1, T2), N1 is N-1.



% EXERCISE 13
% revlist (L, R)
revlist([], []).
revlist([H|T], Res):-
	revlist(T, T1),
	append(T1, [H], Res).



% EXERCISE 14
flattenList([], []).
flattenList([[]|T], R) :- !, flattenList(T, R).
flattenList([[H|T]|L], R) :- !, flattenList([H,T | L], R).
flattenList([H|L], [H|R]) :- flattenList(L, R).



% EXERCISE 15
% checklength(L, R)
checklength([], "even").
checklength([_], "odd").
checklength([_,_|T], R):-
	checklength(T, R).



% EXERCISE 16
delete_nth(L,N,R) :-
  N > 0, 
  exclude(index_multiple_of(N,L),L,R).

index_multiple_of(N,L,E):-
  nth1(I,L,E),
  mod(I,N) =:= 0.



% EXERCISE 17
% remove_duplicates(L,X)
remove_duplicates([], []).
remove_duplicates([X], [X]).
remove_duplicates([H1,H2|T1],T2):-
	remove_duplicates([H2|T1],T2), H1 == H2.
remove_duplicates([H1,H2|T1],[H1|T2]):-
	remove_duplicates([H2|T1],T2), H1 \= H2.



% EXERCISE 18
% lastElem(L,X)
lastElem([X], X).
lastElem([_|T], X):- 
	lastElem(T, X).



% EXERCISE 19
% butLast(L,X)
butLast([X,_], X).
butLast([_|T], X):- 
	butLast(T, X).



% EXERCISE 20
% isSet (List)
isSet([]).
isSet([H|T1]):-
	isSet(T1), not(member(H,T1)).
isSet([H|T1]):-
	false, member(H,T1).



% EXERCISE 21
% subset(A,B)
subset([], _).
subset([H1|T1], B):-
	subset(T1, B), member(H1,B).
subset([H1|_], B):-
	false, not(member(H1,B)).



% EXERCISE 22
% subtract(A,B,C)
subtract([], _, []).
subtract([H1|T1], B, [H1|T2]):-
	subtract(T1, B, T2), not(member(H1,B)).
subtract([H1|T1], B, T2):-
	subtract(T1, B, T2), member(H1,B).



% EXERCISE 23 
% equal_sets(A,B) 
% this is actually like permutation
equal_sets([], []).
equal_sets([], []).
equal_sets([H|T], B):-
	equal_sets(T, T1),
	list_element_rest(B, H, T1).



% EXERCISE 24
% isPerm(A,B)

% list_element_rest
list_element_rest([H|T], H, T).
list_element_rest([H|T1], E, [H|T2]):-
	list_element_rest(T1, E, T2).

%isPerm(A,B)
isPerm([], []).
isPerm([H|T], B):-
	isPerm(T, T1),
	list_element_rest(B, H, T1).



% EXERCISE 25
% neg_to_0(L,R)
neg_to_0([], []).
neg_to_0([H1|T1], [0|T2]):- 
	neg_to_0(T1, T2), H1 < 0.
neg_to_0([H1|T1], [H1|T2]):- 
	neg_to_0(T1, T2), H1 >= 0.



% EXERCISE 26
% delElem(L,E,R)
delElem([], _, []).
delElem([H1|T1], E, T2):-
	delElem(T1, E, T2), H1 == E.
delElem([H1|T1], E, [H1|T2]):-
	delElem(T1, E, T2), H1 \= E.



% EXERCISE 27
% facts (database)
father(john,alice). % john is the father of alice
father(john,andrew). % john is the father of andrew
father(will,john). % will is the father of john, so the grandpa of alice and andrew
mother(mary,alice). % mary is the mother of alice
mother(mary,andrew). %mary is the mother of andrew

descendant(X,Y):- father(X,Y).
descendant(X,Y):- mother(X,Y).

parents(X,Y,Z):- 
	father(X,Z),
	mother(Y,Z).

% descendant(john,X).
% parents(X,Y,alice).
